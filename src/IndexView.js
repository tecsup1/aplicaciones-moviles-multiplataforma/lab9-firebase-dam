/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Dimensions} from 'react-native';
let deviceWidth = Dimensions.get('window').width;
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Button,
  TextInput,
  FlatList,
  ImageBackground,
} from 'react-native';

// firebase:
import firebase from './firebase';
require('firebase/firestore');

class IndexView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      name: '',
      email: '',
      mobile: '',
      isLoading: false,
      data_array: [],
    };
  }

  onResult = (res) => {
    console.log('Got Users collection result.');
    // console.log("resultado papu: "+ QuerySnapshot.);
    const res_array = res.docs.map((e) => {
      const probando = {id: e.id, ...e.data()}; // concatena el id en el mismo array
      console.log(probando);
      return {id: e.id, data: e.data()};
    });
    this.setState({data_array: res_array});
  };

  onError = (error) => {
    console.error(error);
  };
  async componentDidMount() {
    this.setState({isLoading: true});
    const db = firebase.firestore();
    db.settings({experimentalForceLongPolling: true});
    const root = await db
      .collection('users')
      .onSnapshot(this.onResult, this.onError);
  }

  storeUser = () => {
    console.log('Click');
    if (this.state.name === '') {
      alert('Fill at least your name');
    } else {
      this.setState({isLoading: true});
      const db = firebase.firestore();
      db.settings({experimentalForceLongPolling: true});

      db.collection('users')
        .add({
          name: this.state.name,
          email: this.state.email,
          mobile: this.state.mobile,
        })
        .then((res) => {
          this.setState({
            id: '',
            name: '',
            email: '',
            mobile: '',
            isLoading: false,
          });
        });
    }
  };

  getUsers = async () => {
    this.setState({isLoading: true});
    const db = firebase.firestore();

    db.settings({experimentalForceLongPolling: true});

    const root = await db.collection('users').get();
    const res_array = root.docs.map((e) => {
      const probando = {id: e.id, ...e.data()}; // concatena el id en el mismo array
      console.log(probando);
      return {id: e.id, data: e.data()};
    });
    // const res_array = root.docs.map((e) => e.data() );

    this.setState({data_array: res_array});
    console.log('hola2: ', data_array);
    console.log(res_array);
  };

  deleteUser = async () => {
    this.setState({isLoading: true});
    const db = firebase.firestore();
    db.settings({experimentalForceLongPolling: true});
    try {
      const root = await db.collection('users').doc(this.state.id).delete();
      console.log('Usuario eliminado!');
      this.setState({
        isLoading: false,
        id: '',
        name: '',
        email: '',
        mobile: '',
      });
    } catch (e) {
      console.log('error borrando usuario: ' + e);
    }
  };

  updateUser = async () => {
    this.setState({isLoading: true});
    const db = firebase.firestore();

    db.settings({experimentalForceLongPolling: true});
    
    try {
      await db.collection('users').doc(this.state.id).update({
        name: this.state.name,
        email: this.state.email,
        mobile: this.state.mobile,
      });
      this.setState({
        isLoading: false,
        id: '',
        name: '',
        email: '',
        mobile: '',
      });
    } catch (e) {
      console.log('error actualizando usuario: ' + e);
    }
  };

  render() {
    // console.log('por aquí pasamos');
    const image = {
      uri: 'https://cdn.wallpapersafari.com/83/15/xVSYno.jpg',
    };

    return (
      <View style={{flex: 1}}>
        <ImageBackground source={image} style={my_styles.image}>
          <View
            style={{
              width: 500,
              alignItems: 'center',
              marginVertical: 50,
              flex: 1,
            }}>
            <Text style={{color: 'white', fontSize: 30}}>
              USUARIOS FIREBASE
            </Text>
            <Text style={{color: 'white', fontSize: 18}}>Añadir usuario:</Text>
            <TextInput
              style={my_styles.input_text}
              onChangeText={(text) => this.setState({name: text})}
              placeholder="Name"
              value={this.state.name}
            />
            <TextInput
              style={my_styles.input_text}
              onChangeText={(text) => this.setState({email: text})}
              placeholder="Email"
              value={this.state.email}
            />
            <TextInput
              onChangeText={(text) => this.setState({mobile: text})}
              placeholder="Mobile"
              style={[my_styles.input_text, {marginBottom: 15}]}
              value={this.state.mobile}
            />
            <Button title="ADD USER" onPress={this.storeUser} />
            <Button
              title="Limpiar campos"
              onPress={() => {
                this.setState({
                  id: '',
                  name: '',
                  email: '',
                  mobile: '',
                });
              }}
            />
            <Button title="Borrar usuario" onPress={this.deleteUser} />
            <Button title="Actualizar usuario" onPress={this.updateUser} />
          </View>

          <View
            style={{
              width: 500,
              alignItems: 'center',
              marginVertical: 50,
              flex: 2,
            }}>
            <Text style={{color: 'white', fontSize: 20}}>Listar usuarios:</Text>
            <Button title="Cargar lista:" onPress={this.getUsers} />
            <FlatList
              data={this.state.data_array}
              extraData={this.state.data_array} // permite refrescar rápidamente la data del flatlist
              style={{marginVertical: 15, flex: 1}}
              renderItem={({item}) => (
                <TouchableOpacity
                  style={{
                    backgroundColor: 'lightblue',
                    padding: 10,
                    borderRadius: 15,
                    margin: 5,
                    height: 80,
                    width: 200,
                    paddingLeft: 15,
                    shadowColor: '#000',
                    shadowOffset: {
                      width: 0,
                      height: 2,
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,
                    elevation: 7,
                  }}
                  onPress={() => {
                    this.setState({
                      id: item.id,
                      name: item.data.name,
                      email: item.data.email,
                      mobile: item.data.mobile,
                    });
                    console.log(this.state.id);
                  }}>
                  <View>
                    <Text style={{color: 'black', fontSize: 20}}>
                      {item.data.name}
                    </Text>
                    <Text style={{color: 'black'}}>{item.data.email}</Text>
                    <Text style={{color: 'black'}}>{item.data.mobile}</Text>
                  </View>
                </TouchableOpacity>
              )}
              contentContainerStyle={{paddingBottom: 0}}
              // keyExtractor={(item, index) => item.restaurant_id.toString()}
            />
          </View>
        </ImageBackground>
      </View>
    );
  }
}
const my_styles = StyleSheet.create({
  input_text: {
    width: 200,
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    margin: 5,
    alignSelf: 'center',
    backgroundColor: 'white',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});
export default IndexView;
