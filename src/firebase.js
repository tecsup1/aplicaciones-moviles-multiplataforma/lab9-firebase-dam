import firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyBZ6oVwSNvNrrz4ymwIlzbQlhJ42vwyL8c',
  authDomain: 'project9-dam.firebaseapp.com',
  databaseURL: 'https://project9-dam.firebaseio.com',
  projectId: 'project9-dam',
  storageBucket: 'project9-dam.appspot.com',
  messagingSenderId: '793226959013',
  appId: '1:793226959013:web:85ce638f11be3f078fd54c',
};

// Initialize firebase
firebase.initializeApp(firebaseConfig);

export default firebase;
