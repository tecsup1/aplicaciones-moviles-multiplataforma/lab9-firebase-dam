/**
 * @format
 */
console.disableYellowBox = true;
import {AppRegistry} from 'react-native';
// import App from './App';
import IndexView from './src/IndexView';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => IndexView);
